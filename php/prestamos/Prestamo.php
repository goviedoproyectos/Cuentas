<?php

	class Prestamo {

		public $nombre = null;
		public $valor = 0;
		public $descripcion = "Sin comentarios";
		public $conexion = null;
		public $servidor = "localhost";
		public $usuario = "goviedo";
		public $password = "1ciglome";
		public $bd = "gastos";
		public $id = 0;
		
		private function conectar() {
			//$this->conexion = new mysqli($this->servidor, $this->usuario, $this->password, $this->bd); 
			$this->conexion = new PDO("mysql:host=$this->servidor;dbname=$this->bd",$this->usuario,$this->password);
			$this->conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}

		function __construct() {
			$this->conectar();
		}

		public static function ingresar($nombre, $valor, $descripcion) {
			$instance = new self();
			$instance->nombre = $nombre;
			$instance->valor = $valor;
			$instance->descripcion = $descripcion;
			return $instance;
		}

		public static function eliminarId($id) {
			$instance = new self();
			$instance->id = $id;
			return $instance;
		}

		private function ejecutar($sql) {
		 try {
		 
			$this->conexion->exec($sql);

			echo "--Ejecutado--\r\n";

		 } catch(PDOException $e) {
			echo "Error no pude ejecutar:".$e->getMessage();
		 }

		 $this->conexion = null;
		}
	
		public function nuevo() {

		 $sql = "insert into prestamos (nombre, valor, descripcion) values('".$this->nombre."',".$this->valor.",'".$this->descripcion."');"; 
		 $this->ejecutar($sql);

		}

		public function eliminar() {
			// En realidad no es una eliminacion del registro, sino que es un registro a la fecha de la devolucion.
		// Esta fecha corresponde no a la fecha devuelta, sino que a la fecha en que yo registro el pago. Por eso es now()
			$sql = "update prestamos set fecha_devolucion = now() where id = ".$this->id;
			$this->ejecutar($sql);
		}
	}
?>
