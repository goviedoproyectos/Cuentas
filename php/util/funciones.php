<?php
	function valoresEntreHoyYelDiaPago($dbh,$diaDePago) {

		$sql = 'Select date_format( last_day(date_add(now(), INTERVAL -1 MONTH)) ,"%d") as fecha';

		$stmt = $dbh->prepare($sql);

		$stmt->execute();

		$resultado = $stmt->fetchColumn();

		$ultimo_dia_mes_anterior = intval($resultado);

		$diasArestar = $diaDePago-$ultimo_dia_mes_anterior;

		$sql = 'select date_add(  last_day(date_add(now(),INTERVAL -1 MONTH)), INTERVAL '.$diasArestar.' DAY)';

		$stmt = $dbh->prepare($sql);

		$stmt->execute();

		$resultado = $stmt->fetchColumn();

		//Obtenemos los resultados entre la fecha de hoy y el mes de pago (dia 28)
		$sql = "select id,nombre,valor from gastos where fecha between '".$resultado."' and now()";

		$stmt = $dbh->prepare($sql);

		$stmt->execute();

		$arregoConResultados = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $arregoConResultados;
	}

	function todosLosGastos($dbh) {

		$sql = 'Select id, nombre, valor from gastos';
		$stmt = $dbh->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
?>
