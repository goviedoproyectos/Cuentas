(function() {

  var gastosFactory = function($http) {

    var factory = {};

    factory.listaGastos = function() {
      var url = window.location.href+"php/gastos.php?callback=JSON_CALLBACK";
      return $http.jsonp(url);
    }

    return factory;
  }

  gastosFactory.$inject = ['$http'];

  angular.module('GastosApp').factory('gastosFactory', gastosFactory);
}());
