(function() {
	var LoadController = function($scope) {
		$scope.$on('LOAD', function() {
			$scope.loading = true;
		});

		$scope.$on('UNLOAD', function() {
			$scope.loading = false;
		});
	};

  LoadController.$inject = ['$scope'];
  angular.module('GastosApp').controller('LoadController', LoadController);
}());
