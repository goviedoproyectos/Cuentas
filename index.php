<!DOCTYPE html>
<html ng-app="GastosApp">
  <title>Gastos MI Maestro!</title>
<head>
  <!-- Responsive -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="bower_components/angular/angular.js"></script>

  <script src="app/app.js"></script>
  <script src="app/services/gastosFactory.js"></script>
  <script src="app/controllers/LoadController.js"></script>
  <script src="app/controllers/GastosController.js"></script>

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
    <script type="text/javascript">
        var searchWinHref = window.location.href;
        if(searchWinHref.indexOf("?ckattempt") > -1) {
            window.location.href = window.location.href.split('?')[0];
    }
    </script>
<div ng-controller="LoadController">
	<div class="alert alert-info" ng-show="loading">Wait...It is a just a Fucking little Moment...</div>
	<div ng-controller="GastosController" ng-hide="loading">

		<form class="form-horizontal">
		<fieldset>

		<!-- Form Name -->
		<legend>Mis Gastos. Mas Control. Mas Lucas!</legend>

		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Concepto</label>
			<div class="col-md-6">
			<input id="textinput" name="textinput" type="text" placeholder="Que compraste YA!!!!" class="form-control input-md" ng-model="concepto">
			<span class="help-block">Que describa lo que esta comprando, que espero que sea bueno.</span>
			</div>
		</div>

		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="valor">Valor</label>
			<div class="col-md-4">
			<input id="valor" name="valor" type="text" placeholder="Si, seguro?, lo necesitas?." class="form-control input-md" ng-model="valor">
			<span class="help-block">Valor de la compra. PIENSALO, Lo necesito realmente?</span>
			</div>
		</div>

		<!-- Multiple Radios (inline) -->
		<div class="form-group">
			<label class="col-md-4 control-label" for="radios">Fijo o Variable</label>
			<div class="col-md-4">
				<label class="radio-inline" for="radios-1">
					<input type="radio" name="radios" id="radios-1" ng-value="true" ng-checked="true" ng-model="tipo">
					Variable
				</label>
				<label class="radio-inline" for="radios-0">
					<input type="radio" name="radios" id="radios-0" ng-value="false" ng-model="tipo">
					Fijo
				</label>
			</div>
		</div>

		<!-- Button -->
		<div class="form-group">
			<label class="col-md-4 control-label" for="go"></label>
			<div class="col-md-4">
				<button id="go" name="go" class="btn btn-danger" ng-click="ingreso()">GO!</button>
				<button id="reload" name="reload" class="btn btn-info" ng-click="reload()">Reload!</button>
			</div>
		</div>

		</fieldset>
		</form>

		<div class="container">
        <div class="table-responsive">
            <table class="table">
              <tbody>
                <tr>
                  <td><h2>Gastos</h2></td>
                  <td><h3>Ya van {{total | nfcurrency }}</h3></td>
                  <td><h3>Te queda {{1020000 - total | nfcurrency }}</h3></td>
                </tr>
				      </tbody>
            </table>
        </div>
				<div class="table-responsive">
				<table class="table">
						<thead>
								<tr>
										<th>Cual</th>
										<th>Valor</th>
								</tr>
						</thead>
						<tbody>
								<tr ng-repeat="gasto in gastos">
										<td>{{ gasto.nombre}} </td>
										<td>{{ gasto.valor}} </td>
										<!-- http://stackoverflow.com/questions/17039926/adding-parameter-to-ng-click-function-inside-ng-repeat-doesnt-seem-to-work -->
										<td><button id="eliminar" name="eliminar" class="btn btn-warning" ng-click="eliminar(gasto.id)">Bye Fucker!</button></td>
								</tr>
						</tbody>
				</table>
				</div>
		</div>

	</div>
</div>
</body>

</body>
</html>
